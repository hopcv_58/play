package models;
 
import play.data.validation.Constraints;
import java.util.*;
import play.db.ebean.Model;
import javax.persistence.*;

@Entity
public class Tag extends Model {
	@Id
    public Long id;
    public String name;
	@ManyToMany(mappedBy = "tags")
    public List<Product> products;
	public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);
    public static Tag findById(Long id) {
        return find.byId(id);
    }
}