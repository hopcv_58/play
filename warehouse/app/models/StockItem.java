
package models;
import models.*;
import play.db.ebean.Model;
import javax.persistence.*;
@Entity
public class StockItem extends Model{
    @Id
    public Long id;
	@ManyToOne
	public Warehouse warehouse;
 
    @ManyToOne
    public Product product;
 
    public Long quantity;
	public static Finder<Long, Tag> find = new Finder<>(Long.class, Tag.class);
    public String toString() {
        return String.format("%d %s", quantity, product);
    }
}